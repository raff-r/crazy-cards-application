const seleniumServer = require('selenium-server');
const chromedriver = require('chromedriver');

require('nightwatch-cucumber')({
  cucumberArgs: [
    '--require', 'hooks.js',
    '--require', 'timeout.js',
    '--require', 'tests/e2e/step_definitions',
    '--format', 'node_modules/cucumber-pretty',
    '--format', 'json:reports/cucumber.json',
    'tests/e2e/features'
  ]
});

module.exports = {
  src_folders : ["tests/e2e"],
  output_folder: 'reports',
  custom_assertions_path: '',
  live_output: false,
  disable_colors: false,
  page_objects_path: './tests/e2e/page_objects',
  custom_assertions_path: './tests/e2e/customAssertions',
  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    log_path: '',
    host: '127.0.0.1',
    port: 4444
  },
  test_settings: {
    default: {
      launch_url: 'http://localhost:3000',
      selenium_port: 4444,
      selenium_host: '127.0.0.1',
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true
      },
      selenium: {
        cli_args: {
          'webdriver.chrome.driver': chromedriver.path
        }
      },
      screenshots : {
        enabled : true,
        on_failure : true,
        path: 'screenshots/chrome'
      }
    },
    integration : {
      launch_url: 'http://localhost:3000',
      desiredCapabilities: {
        chromeOptions : {
          args : [
            "headless",
            "no-sandbox",
            "disable-gpu",
            "window-size=1280,800"
          ]
        }
      },
      selenium: {
        cli_args: {
          'webdriver.chrome.driver': chromedriver.path
        }
      },
      screenshots : {
        enabled : true,
        on_failure : true,
        path: 'screenshots/chrome'
      }
    },
    chromeHeadless: {
      desiredCapabilities: {
        chromeOptions : {
          args : [
            "headless",
            "no-sandbox",
            "disable-gpu",
            "window-size=1280,800"
          ]
        }
      },
      selenium: {
        cli_args: {
          'webdriver.chrome.driver': chromedriver.path
        }
      },
      screenshots : {
        enabled : true,
        on_failure : true,
        path: 'screenshots/chrome'
      }
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
  }
};
