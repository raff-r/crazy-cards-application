var reporter = require('cucumber-html-reporter');
var data = require('./package.json');

var options = {
  theme: 'bootstrap',
  jsonFile: 'reports/cucumber.json',
  output: 'reports/cucumber_report.html',
  reportSuiteAsScenarios: true,
  launchReport: false,
  metadata: {
    versionNumber: data.version,
    url: data.url
  }
};

reporter.generate(options);