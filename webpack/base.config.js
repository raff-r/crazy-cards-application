const path = require('path');
const paths = require('./paths');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

// Webpack configuration
module.exports = {
  entry: path.join(paths.JS, 'index.js'),
  output: {
    path: paths.DIST,
    filename: 'app.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ],
      },
      {
        test: /\.(css|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader?url=false',
              options: {
                minimize: true || {/* CSSNano Options */}
              }
            },
            {
              loader: 'postcss-loader'
            },
            {
              loader: 'sass-loader'
            }
          ]
        })
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000
        }
      }
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.DefinePlugin({
        'process.env.TOOL_VERSION': JSON.stringify(process.env.npm_package_version),
        'process.env.TOOL_NAME': JSON.stringify(process.env.npm_package_name)
      })
  ]
};
