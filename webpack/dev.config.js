const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const paths = require('./paths');

module.exports = merge(baseConfig, {
  devtool: 'eval-source-map',
  entry: path.join(paths.JS, 'index.js'),
  output: {
    path: paths.DIST,
    filename: 'app.bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(paths.PUBLIC, 'index.html'),
    }),
    new ExtractTextPlugin('index.css')
  ],
  devServer: {
    inline: true,
    contentBase: 'src',
    port: '3000',
    watchOptions: {
      poll: 100
    }
  }
});
