const path = require('path');

module.exports = {
  SRC: path.resolve(__dirname, '../src'),
  JS: path.resolve(__dirname, '../src/js'),
  PUBLIC: path.resolve(__dirname, '../src/public'),
  DIST: path.resolve(__dirname, '../dist'),
};
