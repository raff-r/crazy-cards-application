const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const baseConfig = require('./base.config.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./paths');

module.exports = merge(baseConfig, {
  output: {
    path: paths.DIST,
    filename: 'scripts/scripts.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/public/markup.html',
      filename: 'markup.html'
    }),
    // Extract imported CSS into own file
    new ExtractTextPlugin({
      filename: 'styles/styles.css'
    }),

    //Minify JS
    new UglifyJsPlugin({
      sourceMap: false
    }),
    //Minify CSS
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
    new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new CopyWebpackPlugin([
      {  context: paths.PUBLIC, from: "files", to: path.join(paths.DIST, 'files/'), ignore: [ '*.md' ]},
      {  context: paths.PUBLIC, from: "images", to: path.join(paths.DIST, 'images/'), ignore: [ '*.md' ]},
      {  context: paths.PUBLIC, from: "fonts", to: path.join(paths.DIST, 'fonts/'), ignore: [ '*.md' ]},
      {  context: paths.PUBLIC, from: "css.txt", to: paths.DIST},
      {  context: paths.PUBLIC, from: "js.txt", to: paths.DIST}
    ]),
    new CleanWebpackPlugin(['../dist'], {allowExternal: true})
  ]
});
