# Crazy Cards Application

## Setup

After cloning the repo run `yarn install` or `npm install`

Once all modules have installed to view the application run `yarn start` or `npm run start`

##Unit Tests

The application has been set up to run unit tests using Jest with enzyme. To run unit tests run `yarn test` or `npm run test`. 

A coverage report is generated if you run `yarn test:coverage` or `npm run test:coverage`. The coverage report can be found in /coverage/lcov-report/index.html

##End to End Tests

The application has been set up to run end to end tests using Nightwatch with Cucumber. To view the tests in Chrome run `yarn start' or `npm run start. Once a local server is running in another terminal window run `yarn e2e` or `npm run e2e`.

Alternatively if you wish to run the tests in headless chrome run `yarn e2e:ci` or `npm run e2e:ci`. This will automatically setup a local dev server so **NO** need to run `yarn start`.

A report is automatically generated in /reports/cucumber_report.html.