import React from "react";
import { render } from "react-dom";
import Index from "../../src/js/index.js"; // Required to call react dom render
import App from "../../src/js/components/App";

jest.mock("react-dom");

describe("index.js", () => {
  it("should render correctly", () => {
    Index; // Required to call react dom render
    expect(render.mock.calls[0][0]).toEqual(<App />);
    expect(render).toHaveBeenCalledTimes(1);
  });
});
