import React from "react";
import ApplicationForm from "../../src/js/components/ApplicationForm";

describe("ApplicationForm Component: ", () => {
  let renderComponent;

  beforeAll(() => {
    renderComponent = () => {
      return shallow(<ApplicationForm onSubmit={jest.fn()} />);
    };
  });

  it("should match the snapshot", () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent).toMatchSnapshot();
  });
});
