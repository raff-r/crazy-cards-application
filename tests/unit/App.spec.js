import React from "react";
import App from "../../src/js/components/App";
import animateScrollTo from "animated-scroll-to";

jest.useFakeTimers();
jest.mock("animated-scroll-to");

describe("Main App Component: ", () => {
  let renderComponent;

  beforeAll(() => {
    renderComponent = () => {
      return shallow(<App />);
    };
  });

  it("should match the snapshot", () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent).toMatchSnapshot();
  });

  describe("Submitting the form", () => {
    let submitForm;

    beforeAll(() => {
      renderComponent = () => {
        return mount(<App />);
      };

      submitForm = (
        component,
        title,
        firstName,
        lastName,
        dob,
        annualIncome,
        employmentStatus,
        houseNumber,
        postCode
      ) => {
        const Form = component.find("Form");
        const titleInput = component.find("Dropdown#title");
        const firstNameInput = component.find("Input#firstName");
        const lastNameInput = component.find("Input#lastName");
        const dobInput = component.find("Input#dob");
        const incomeInput = component.find("Input#annualIncome");
        const employmentStatusInput = component.find(
          "Dropdown#employmentStatus"
        );
        const houseNumberInput = component.find("Input#houseNumber");
        const postCodeInput = component.find("Input#postCode");

        titleInput.props().setValue(title);
        firstNameInput.props().setValue(firstName);
        lastNameInput.props().setValue(lastName);
        dobInput.props().setValue(dob);
        incomeInput.props().setValue(annualIncome);
        employmentStatusInput.props().setValue(employmentStatus);
        houseNumberInput.props().setValue(houseNumber);
        postCodeInput.props().setValue(postCode);
        Form.simulate("submit");
      };
    });

    afterEach(() => {
      jest.clearAllTimers();
    });

    it("should call animate scroll to when the form is successfully submitted", () => {
      const renderedComponent = renderComponent();
      submitForm(
        renderedComponent,
        "Mr",
        "Ollie",
        "Murphree",
        "01/07/1970",
        "34000",
        "Full Time",
        "700",
        "BS14 9PR"
      );

      jest.runAllTimers();
      expect(animateScrollTo).toHaveBeenCalledTimes(1);
    });
  });
});
