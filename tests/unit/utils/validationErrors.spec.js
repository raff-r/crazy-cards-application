import validationErrors from "../../../src/js/utils/validationErrors";

describe("Validation Error Messages: ", () => {
  it("should have default error messages set for required error", () => {
    expect(validationErrors.hasOwnProperty("required")).toBeTruthy();
    expect(validationErrors.required).toBe("This is required");
  });

  it("should have default error messages set for numeric error", () => {
    expect(validationErrors.hasOwnProperty("numericError")).toBeTruthy();
    expect(validationErrors.numericError).toBe("This should be a number");
  });
});
