import getResults from "../../../src/js/utils/getResults";

describe("Get results array: ", () => {
  it("should always return the Anywhere card", () => {
    let anywhereCard = getResults("Full Time", 25000).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Part Time", 10).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Self Employed", 87000).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Homemaker", 0).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Retired", 4000).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Unemployed", 2000).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Student", 12000).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();

    anywhereCard = getResults("Other", 12000).find(
      card => card.name === "Anywhere Card"
    );
    expect(!!anywhereCard).toBeTruthy();
  });

  it("should return the Student Life Card when a user sets Student as Student ", () => {
    let studentCard = getResults("Student", 25000).find(
      card => card.name === "Student Life Card"
    );
    expect(!!studentCard).toBeTruthy();

    studentCard = getResults("Full Time", 25000).find(
      card => card.name === "Student Life Card"
    );
    expect(!!studentCard).toBeFalsy();
  });

  it("should return the Liquid Card when a user sets annual income greater than 16000 ", () => {
    let liquidCard = getResults("Student", 25000).find(
      card => card.name === "Liquid Card"
    );
    expect(!!liquidCard).toBeTruthy();

    liquidCard = getResults("Unemployed", 2000).find(
      card => card.name === "Liquid Card"
    );
    expect(!!liquidCard).toBeFalsy();
  });

  it("should return the correct number of results based on the users input", () => {
    expect(getResults("Student", 25000).length).toEqual(3);
    expect(getResults("Student", 14000).length).toEqual(2);
    expect(getResults("Full Time", 75000).length).toEqual(2);
    expect(getResults("Full Time", 20000).length).toEqual(2);
    expect(getResults("Part Time", 2000).length).toEqual(1);
    expect(getResults("Unemployed", 2000).length).toEqual(1);
    expect(getResults("Homemaker", 0).length).toEqual(1);
  });
});
