import React from "react";
import animateScrollTo from "animated-scroll-to";

import Form from "../../../src/js/components/FormComponents/Form";
import Input from "../../../src/js/components/FormComponents/Input";

jest.useFakeTimers();
jest.mock("animated-scroll-to");

describe("Form Component: ", () => {
  let renderComponent;
  let onValidSubmit;

  describe("No inputs: ", () => {
    beforeAll(() => {
      renderComponent = props => {
        return shallow(
          <Form {...props}>
            <p>Inputs go here</p>
          </Form>
        );
      };
    });

    beforeEach(() => {
      onValidSubmit = jest.fn();
    });

    it("should match the snapshot", () => {
      const component = renderComponent({ onValidSubmit });
      expect(component).toMatchSnapshot();
    });
  });

  describe("Valid submit: ", () => {
    beforeAll(() => {
      renderComponent = props => {
        return mount(
          <Form {...props}>
            <Input
              name="input1"
              id="input1"
              type="text"
              value="test"
              label="this is the label"
            />
          </Form>
        );
      };
    });

    beforeEach(() => {
      onValidSubmit = jest.fn();
    });

    afterEach(() => {
      onValidSubmit.mockClear();
      jest.clearAllTimers();
    });

    it("should call the onValidSubmit function when successfully submitting the form", () => {
      const component = renderComponent({ onValidSubmit });
      component.simulate("submit");
      expect(onValidSubmit).toHaveBeenCalledTimes(1);
    });
  });

  describe("Invalid submit: ", () => {
    beforeAll(() => {
      renderComponent = props => {
        return mount(<Form {...props} />);
      };
    });

    beforeEach(() => {
      onValidSubmit = jest.fn();
    });

    afterEach(() => {
      onValidSubmit.mockClear();
      animateScrollTo.mockClear();
      jest.clearAllTimers();
    });

    it("should call animateScrollTo when a form is submitted and has errors", () => {
      const component = renderComponent({
        onValidSubmit,
        children: (
          <div>
            <Input
              name="input1"
              id="input1"
              type="email"
              value="test"
              validations="isEmail"
              validationError="This is not an email"
              label="this is the label"
            />
            <Input
              name="input2"
              id="input1"
              type="text"
              value="test"
              required
              label="this is the label"
            />
          </div>
        )
      });
      component.simulate("submit");
      jest.runAllTimers();
      expect(animateScrollTo).toHaveBeenCalledTimes(1);
    });
  });
});
