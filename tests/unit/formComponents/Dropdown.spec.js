import React from "react";
import Dropdown from "../../../src/js/components/FormComponents/Dropdown";
import Formsy from "formsy-react";

describe("Dropdown Component: ", () => {
  let renderComponent;
  let renderedComponent;

  beforeAll(() => {
    renderComponent = props => {
      return mount(
        <Formsy>
          <Dropdown {...props} />
        </Formsy>
      );
    };
  });

  beforeEach(() => {
    renderedComponent = renderComponent({
      type: "number",
      name: "annualIncome",
      label: "Annual Income",
      id: "annualIncome",
      options: ["Mr", "Mrs", "Miss", "Ms", "Dr"]
    });
  });

  it("should contain all the elements needed to style an Dropdown component", () => {
    expect(renderedComponent.find(".form-group")).toHaveLength(1);
    expect(renderedComponent.find("Label")).toHaveLength(1);
    expect(renderedComponent.find(".input-group")).toHaveLength(1);
    expect(renderedComponent.find(".form-control")).toHaveLength(1);
    expect(renderedComponent.find("ErrorMessage")).toHaveLength(1);
  });

  it("should render the correct attributes on the input element", () => {
    const input = renderedComponent.find("select");

    expect("className" in input.props()).toBeTruthy();
    expect("id" in input.props()).toBeTruthy();
    expect("onFocus" in input.props()).toBeTruthy();
    expect("onBlur" in input.props()).toBeTruthy();
    expect("onChange" in input.props()).toBeTruthy();
    expect("value" in input.props()).toBeTruthy();
    expect("defaultValue" in input.props()).toBeTruthy();
  });

  describe("Callback event testing: with values", () => {
    let eventFn;

    beforeEach(() => {
      eventFn = jest.fn();

      renderedComponent = renderComponent({
        label: "First Name",
        placeholder: "Placeholder text",
        name: "firstname",
        id: "firstname",
        type: "text",
        handleChange: eventFn,
        handleBlur: eventFn,
        handleFocus: eventFn,
        options: ["Mr", "Mrs", "Miss", "Ms", "Dr"]
      });
    });

    it("should call a function when passed into handleFocus and the field gains focus", () => {
      renderedComponent.find("select").simulate("focus");
      expect(eventFn).toHaveBeenCalled();
    });

    it("should call a function when passed into handleChange and the field is changed", () => {
      renderedComponent.find("select").simulate("change");
      expect(eventFn).toHaveBeenCalled();
    });

    it("should call a function when passed into handleBlur and the field gains blur", () => {
      renderedComponent.find("select").simulate("blur");
      expect(eventFn).toHaveBeenCalled();
    });
  });

  describe("Callback event testing: without values", () => {
    let eventFn;

    beforeEach(() => {
      eventFn = jest.fn();

      renderedComponent = renderComponent({
        label: "First Name",
        placeholder: "Placeholder text",
        name: "firstname",
        id: "firstname",
        type: "text",
        options: ["Mr", "Mrs", "Miss", "Ms", "Dr"]
      });
    });

    it('should not a function when nothing is passed into the "handleFocus" prop and the onFocus event is called', () => {
      renderedComponent.find("select").simulate("focus");
      expect(eventFn).toHaveBeenCalledTimes(0);
    });

    it('should not call a function when nothing is passed into the "handleChange" prop and onChange event is called', () => {
      renderedComponent.find("select").simulate("change");
      expect(eventFn).toHaveBeenCalledTimes(0);
    });

    it('should not call a function when nothing is passed into the "handleBlur" prop and the onBlur event is called', () => {
      renderedComponent.find("select").simulate("blur");
      expect(eventFn).toHaveBeenCalledTimes(0);
    });
  });

  describe("Validation state: ", () => {
    let eventFn;

    beforeEach(() => {
      eventFn = jest.fn();

      renderedComponent = renderComponent({
        label: "First Name",
        placeholder: "Placeholder text",
        name: "firstname",
        id: "firstname",
        type: "text",
        required: true,
        options: ["Mr", "Mrs", "Miss", "Ms", "Dr"]
      });
    });

    it("should render the input as invalid when the Dropdown is invalid and form is not pristine", () => {
      renderedComponent.find("select").simulate("blur");

      expect(renderedComponent.find(".is-invalid")).toHaveLength(1);
      expect(renderedComponent.find(".invalid-feedback")).toHaveLength(1);
    });
  });
});
