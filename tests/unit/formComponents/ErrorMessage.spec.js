import React from "react";
import ErrorMessage from "../../../src/js/components/FormComponents/ErrorMessage";

describe("Error Message Component: ", () => {
  let renderComponent;

  beforeAll(() => {
    renderComponent = props => {
      return shallow(<ErrorMessage {...props} />);
    };
  });

  it("should render the Error Message Component when the input is invalid and the form is not pristine", () => {
    const renderedComponent = renderComponent({
      isValid: false,
      errorMessage: "This is the error message",
      isPristine: false
    });

    expect(renderedComponent.text()).toBe("This is the error message");
    expect(renderedComponent.text()).not.toBeNull;
  });

  it("should not render the Error Message Component when the input is valid and the form is pristine", () => {
    const renderedComponent = renderComponent({
      isValid: true,
      errorMessage: "This is the error message",
      isPristine: true
    });

    expect(renderedComponent.text()).toBeNull;
  });

  it("should not render the Error Message Component when the input is valid and the form is not pristine", () => {
    const renderedComponent = renderComponent({
      isValid: true,
      errorMessage: "This is the error message",
      isPristine: false
    });

    expect(renderedComponent.text()).toBeNull;
  });
});
