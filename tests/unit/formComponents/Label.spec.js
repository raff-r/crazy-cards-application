import React from "react";
import Label from "../../../src/js/components/FormComponents/Label";

describe("Label Component: ", () => {
  let renderComponent;
  let renderedComponent;

  beforeAll(() => {
    renderComponent = props => {
      return shallow(<Label {...props} />);
    };
  });

  beforeEach(() => {
    renderedComponent = renderComponent({
      htmlFor: "labelID",
      label: "This is the label"
    });
  });

  it("should have an attribute htmlFor on the label", () => {
    expect(renderedComponent.prop("htmlFor")).toBe("labelID");
  });

  it("should set the correct text for the label", () => {
    expect(renderedComponent.text()).toBe("This is the label");
  });
});
