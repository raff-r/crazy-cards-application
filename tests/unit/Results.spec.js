import React from "react";
import Results from "../../src/js/components/Results";

describe("Results Component: ", () => {
  let renderComponent;

  beforeAll(() => {
    renderComponent = results => {
      return shallow(<Results results={results} />);
    };
  });

  it("should match the snapshot", () => {
    const renderedComponent = renderComponent([
      {
        name: "Anywhere Card",
        apr: 33.9,
        offerDuration: 0,
        purchaseDuration: 0,
        credit: 30
      }
    ]);

    expect(renderedComponent.find(".results")).toHaveLength(1);
  });

  it("should not render results component if no results are passed in", () => {
    const renderedComponent = renderComponent([]);
    expect(renderedComponent.find(".results")).toHaveLength(0);
  });
});
