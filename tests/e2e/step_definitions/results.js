const { client } = require("nightwatch-cucumber");
const { Given, When, Then } = require("cucumber");

var page = client.page.results();

Given("I am on the page", function() {
  return page.navigate().waitForElementVisible("body", 2000);
});

When(/^I populate the (.*) dropdown with (.*)$/, function(input, data) {
  return page.selectDropdown(page.elements[input].selector, data);
});

When(/^I populate the (.*) input with (.*)$/, function(input, data) {
  return page.setValue("@" + input, data);
});

When("I submit the form", () => {
  return page.submitToolForm("@submitButton");
});

Then("I should see the correct {string} for the user", function(cards) {
  let cardsArray = cards.split(", ");

  let temp;
  for (var i = 0; i < cardsArray.length; i++) {
    temp =
      temp &&
      page
        .waitForElementVisible("@resultsBox", 2000)
        .assert.containsText("@resultsBox", cardsArray[i]);
  }
  return temp;
});
