var util = require("util");

module.exports = {
  url: function() {
    return this.api.launchUrl;
  },
  elements: {
    title: {
      selector: "#title option[value=%s]"
    },
    firstName: {
      selector: "#firstName"
    },
    lastName: {
      selector: "#lastName"
    },
    dob: {
      selector: "#dob"
    },
    income: {
      selector: "#annualIncome"
    },
    employmentStatus: {
      selector: "#employmentStatus option[value=%s]"
    },
    houseNumber: {
      selector: "#houseNumber"
    },
    postCode: {
      selector: "#postCode"
    },
    submitButton: {
      selector: "#submit-btn"
    },
    resultsBox: {
      selector: ".results"
    }
  },
  commands: [
    {
      submitToolForm: function(selector) {
        return this.click(selector);
      },
      selectDropdown: function(selector, data) {
        var selectionOption = util.format(selector, data);
        return this.click(selectionOption);
      },
      clears: function(selector) {
        const { RIGHT_ARROW, BACK_SPACE } = this.api.Keys;
        return this.getValue(selector, result => {
          const chars = result.value.split("");
          // Make sure we are at the end of the input
          chars.forEach(() => this.setValue(selector, RIGHT_ARROW));
          // Delete all the existing characters
          chars.forEach(() => this.setValue(selector, BACK_SPACE));
        });
      }
    }
  ]
};
