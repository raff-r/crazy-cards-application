Feature: Totally Money Crazy Cards Application results test

  Scenario Outline: Check if the application displays the correct cards for different users
    Given I am on the page
    When I populate the title dropdown with <title>
    And I populate the firstName input with <firstName>
    And I populate the lastName input with <lastName>
    And I populate the dob input with <dob>
    And I populate the income input with <income>
    And I populate the employmentStatus dropdown with <employmentStatus>
    And I populate the houseNumber input with <houseNumber>
    And I populate the postCode input with <postCode>
    And I submit the form
    Then I should see the correct <cards> for the user

    Examples:
      | title | firstName | lastName  | dob        | income | employmentStatus | houseNumber | postCode | cards                                      |
      | Mr    | Ollie     | Murphree  | 01/07/1970 | 34000  | "Full Time"        | 700         | BS14 9PR | "Anywhere Card, Liquid Card"               |
      | Miss  | Elizabeth | Edmundson | 29/06/1984 | 17000  | "Student"          | 177         | PH12 8UW | "Anywhere Card, Liquid Card, Student Card" |
      | Mr    | Trevor    | Rieck     | 07/09/1990 | 15000  | "Part Time"        | 343         | TS25 2NF | "Anywhere Card"                            |

