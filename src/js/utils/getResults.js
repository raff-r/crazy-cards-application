const getResults = (employmentStatus, annualIncome) => {
  let cards = [
    {
      name: "Anywhere Card",
      apr: 33.9,
      offerDuration: 0,
      purchaseDuration: 0,
      credit: 300
    }
  ];

  if (employmentStatus === "Student") {
    cards.push({
      name: "Student Life Card",
      apr: 18.9,
      offerDuration: 0,
      purchaseDuration: 6
    });
  }

  if (annualIncome > 16000) {
    cards.push({
      name: "Liquid Card",
      apr: 33.9,
      offerDuration: 12,
      purchaseDuration: 6,
      credit: 3000
    });
  }

  return cards;
};

export default getResults;
