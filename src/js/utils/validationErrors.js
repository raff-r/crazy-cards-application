const validationErrors = {
  required: "This is required",
  numericError: "This should be a number"
};

export default validationErrors;
