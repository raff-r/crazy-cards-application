import React from "react";

import Form from "./FormComponents/Form";
import Input from "./FormComponents/Input";
import Dropdown from "./FormComponents/Dropdown";

import validationErrors from "../utils/validationErrors";

const ApplicationForm = props => {
  return (
    <Form onValidSubmit={props.onSubmit}>
      <br />
      <div className="row">
        <div className="col-12 col-md-2">
          <Dropdown
            options={["Mr", "Mrs", "Miss", "Ms", "Dr"]}
            name="title"
            label="Title"
            id="title"
            placeholder="Select Title"
          />
        </div>
        <div className="col-12 col-md-5">
          <Input
            type="text"
            name="firstName"
            label="First Name"
            id="firstName"
            required
            validationErrors={{
              isDefaultRequiredValue: validationErrors.required
            }}
          />
        </div>
        <div className="col-12 col-md-5">
          <Input
            type="text"
            name="lastName"
            label="Last Name"
            id="lastName"
            required
            validationErrors={{
              isDefaultRequiredValue: validationErrors.required
            }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <Input
            type="date"
            name="dob"
            label="Date of Birth"
            id="dob"
            required
            validationErrors={{
              isDefaultRequiredValue: validationErrors.required
            }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-md-6">
          <Input
            type="number"
            name="annualIncome"
            label="Annual Income"
            id="annualIncome"
            addonBefore="£"
            required
            validations={{
              isNumeric: true
            }}
            validationErrors={{
              isDefaultRequiredValue: validationErrors.required,
              isNumeric: validationErrors.numericError
            }}
          />
        </div>
        <div className="col-12 col-md-6">
          <Dropdown
            options={[
              "Full Time",
              "Self Employed",
              "Part Time",
              "Homemaker",
              "Retired",
              "Unemployed",
              "Student",
              "Other"
            ]}
            name="employmentStatus"
            label="Employment Status"
            id="employmentStatus"
            placeholder="Choose..."
            required
            validationErrors={{
              isDefaultRequiredValue: validationErrors.required
            }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-md-6">
          <Input
            type="number"
            name="houseNumber"
            label="House Number"
            id="houseNumber"
          />
        </div>
        <div className="col-12 col-md-6">
          <Input type="text" name="postCode" label="Post Code" id="postCode" />
        </div>
        <div className="col-12 col-md-3 offset-md-9">
          <button
            className="btn btn-primary btn-block btn--large"
            type="submit"
            id="submit-btn"
          >
            Submit
          </button>
        </div>
      </div>
    </Form>
  );
};

export default ApplicationForm;
