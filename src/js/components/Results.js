import React from "react";

import creditCard from "../../images/credit-card.png";

const Results = props => {
  if (props.results.length <= 0) {
    return null;
  }

  return (
    <div className="results">
      <h3 className="results__title">Your results</h3>
      {props.results.map((result, index) => {
        return (
          <div key={`card-${index}`} className="results__credit-card">
            <div className="row">
              <div className="col-12">
                <h5>{result.name}</h5>
              </div>
            </div>
            <div className="row align-items-center">
              <div className="col-12 col-md-3">
                <img
                  src={creditCard}
                  alt="Credit card"
                  className="img-fluid results__image"
                />
              </div>
              <div className="col-12 col-md-9">
                <div className="row align-items-center">
                  <div className="col-6 col-md-3">
                    <div className="result__detail">
                      <strong className="results__detail__value">
                        {result.apr}% APR
                      </strong>
                      <p className="results__detail__heading">
                        Representative % APR (variable)
                      </p>
                    </div>
                  </div>
                  <div className="col-6 col-md-3">
                    <div className="result__detail">
                      <strong className="results__detail__value">
                        {result.offerDuration} months
                      </strong>
                      <p className="results__detail__heading">
                        Balance Transfer Offer Duration
                      </p>
                    </div>
                  </div>
                  <div className="col-6 col-md-3">
                    <div className="result__detail">
                      <strong className="results__detail__value">
                        {result.purchaseDuration} months
                      </strong>
                      <p className="results__detail__heading">
                        Purchase Offer Duration
                      </p>
                    </div>
                  </div>
                  <div className="col-6 col-md-3">
                    <div className="result__detail">
                      <strong className="results__detail__value--highlighted">
                        £{result.credit}
                      </strong>
                      <p className="results__detail__heading--highlighted">
                        Credit Available
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Results;
