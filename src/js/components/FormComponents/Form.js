import React from "react";
import Formsy from "formsy-react";
import { func, node } from "prop-types";
import animateScrollTo from "animated-scroll-to";

const Form = props => {
  const onValidSubmit = data => {
    props.onValidSubmit(data);
  };

  const onInvalidSubmit = () => {
    setTimeout(scrollToError, 0); //timeout required so Formsy react apply error is called first
  };

  const scrollToError = () => {
    const className = ".is-invalid";
    const offset = document.querySelector(className)
      ? document.querySelectorAll(className)[0].getBoundingClientRect().top +
        window.pageYOffset
      : window.pageYOffset;

    animateScrollTo(offset, { speed: 1500 });
  };

  return (
    <Formsy
      {...props}
      onValidSubmit={onValidSubmit}
      onInvalidSubmit={onInvalidSubmit}
      noValidate
    >
      {props.children}
    </Formsy>
  );
};

Form.propTypes = {
  /* a callback function for when the form has been submitted successfully*/
  onValidSubmit: func,
  /* ensure there are elements being rendered*/
  children: node.isRequired
};

export default Form;
