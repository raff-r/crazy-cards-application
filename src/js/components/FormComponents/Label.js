import React from "react";
import { string } from "prop-types";

const Label = props => {
  return <label htmlFor={props.htmlFor}>{props.label}</label>;
};

Label.propTypes = {
  /** input the label is for */
  htmlFor: string.isRequired,
  /** input label */
  label: string.isRequired
};

export default Label;
