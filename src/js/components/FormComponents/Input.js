import React, { Component } from "react";
import { func, string, oneOf } from "prop-types";
import { withFormsy } from "formsy-react";
import classNames from "classnames";

import Label from "./Label";
import ErrorMessage from "./ErrorMessage";

class Input extends Component {
  renderAddon(addon, addonPosition) {
    return (
      <div className={`input-group-${addonPosition}`}>
        <div className="input-group-text">{addon}</div>
      </div>
    );
  }

  handleChange = event => {
    this.props.handleChange ? this.props.handleChange(event) : null;
    this.props.setValue(event.currentTarget.value);
  };

  handleFocus = event => {
    this.props.handleFocus ? this.props.handleFocus(event) : null;
  };

  handleBlur = event => {
    this.props.handleBlur ? this.props.handleBlur(event) : null;
    this.props.setValue(this.props.getValue());
  };

  render() {
    const inputClassNames = classNames("form-control", {
      "is-invalid": !this.props.isValid() && !this.props.isPristine()
    });

    return (
      <div className="form-group">
        <Label htmlFor={this.props.id} label={this.props.label} />
        <div className="input-group">
          {this.props.addonBefore &&
            this.renderAddon(this.props.addonBefore, "prepend")}
          <input
            type={this.props.type}
            className={inputClassNames}
            id={this.props.id}
            placeholder={this.props.placeholder}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            onChange={this.handleChange}
            value={this.props.getValue() || ""}
          />
          {this.props.addonAfter &&
            this.renderAddon(this.props.addonAfter, "append")}
          <ErrorMessage
            isValid={this.props.isValid()}
            errorMessage={this.props.getErrorMessage()}
            isPristine={this.props.isPristine()}
          />
        </div>
      </div>
    );
  }
}

Input.propTypes = {
  /** Input type */
  type: oneOf([
    "color",
    "date",
    "datetime",
    "datetime-local",
    "email",
    "hidden",
    "month",
    "number",
    "password",
    "range",
    "search",
    "tel",
    "text",
    "time",
    "url",
    "week"
  ]).isRequired,
  /** Placeholder message */
  placeholder: string,
  /** The text for the label */
  label: string.isRequired,
  /** Text/symbol to appear before input */
  addonBefore: string,
  /** Text/symbol to appear after input */
  addonAfter: string,
  /** An id for the label */
  id: string.isRequired,
  /** Name is needed for Formsy value */
  name: string.isRequired,
  /** Additional handleBlur functionality */
  handleBlur: func,
  /** Additional handleChange functionality */
  handleChange: func,
  /** Additional handleFocus functionality */
  handleFocus: func
};

export default withFormsy(Input);
