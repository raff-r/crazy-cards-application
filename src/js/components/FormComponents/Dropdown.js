import React, { Component } from "react";
import { func, string, arrayOf } from "prop-types";
import { withFormsy } from "formsy-react";
import classNames from "classnames";

import Label from "./Label";
import ErrorMessage from "./ErrorMessage";

class Dropdown extends Component {
  handleChange = event => {
    this.props.handleChange ? this.props.handleChange(event) : null;
    this.props.setValue(event.currentTarget.value);
  };

  handleFocus = event => {
    this.props.handleFocus ? this.props.handleFocus(event) : null;
  };

  handleBlur = event => {
    this.props.handleBlur ? this.props.handleBlur(event) : null;
    this.props.setValue(this.props.getValue());
  };

  renderOptions(options) {
    return options.map((option, index) => {
      return (
        <option value={option} key={index}>
          {option}
        </option>
      );
    });
  }

  render() {
    const inputClassNames = classNames("form-control", {
      "is-invalid": !this.props.isValid() && !this.props.isPristine()
    });

    return (
      <div className="form-group">
        <Label htmlFor={this.props.id} label={this.props.label} />
        <div className="input-group">
          <select
            className={inputClassNames}
            id={this.props.id}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            onChange={this.handleChange}
            value={this.props.getValue() || ""}
            defaultValue="default"
            value={this.props.getValue()}
          >
            <option disabled={true} value="default" hidden>
              {this.props.placeholder}
            </option>
            {this.renderOptions(this.props.options)}
          </select>
          <ErrorMessage
            isValid={this.props.isValid()}
            errorMessage={this.props.getErrorMessage()}
            isPristine={this.props.isPristine()}
          />
        </div>
      </div>
    );
  }
}

Dropdown.propTypes = {
  /** The select options*/
  options: arrayOf(string).isRequired,
  /** Placeholder message */
  placeholder: string,
  /** The text for the label */
  label: string.isRequired,
  /** An id for the label */
  id: string.isRequired,
  /** Name is needed for Formsy value */
  name: string.isRequired,
  /** Additional handleBlur functionality */
  handleBlur: func,
  /** Additional handleChange functionality */
  handleChange: func,
  /** Additional handleFocus functionality */
  handleFocus: func
};

export default withFormsy(Dropdown);
