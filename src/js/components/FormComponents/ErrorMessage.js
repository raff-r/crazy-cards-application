import React from "react";
import { string, bool } from "prop-types";

const ErrorMessage = props => {
  return !props.isValid && !props.isPristine ? (
    <div className="invalid-feedback">{props.errorMessage}</div>
  ) : null;
};

ErrorMessage.defaultProps = {
  /** Determine if the input is valid or not */
  isValid: bool.isRequired,
  /** Error message to be displayed */
  errorMessage: string,
  /** State to determine if the form has any values changed */
  isPristine: bool.isRequired
};

export default ErrorMessage;
