import React, { Fragment, Component } from "react";
import animateScrollTo from "animated-scroll-to";

import ApplicationForm from "./ApplicationForm";
import Results from "./Results";

import getResults from "../utils/getResults";

import logo from "../../images/logo.svg";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: []
    };
  }

  scrollToResults = () => {
    const className = ".results";
    const offset = document.querySelector(className)
      ? document.querySelectorAll(className)[0].getBoundingClientRect().top +
        window.pageYOffset
      : window.pageYOffset;

    animateScrollTo(offset, { speed: 1500 });
  };

  onSubmit = data => {
    this.setState({
      results: getResults(data.employmentStatus, data.annualIncome)
    });

    setTimeout(this.scrollToResults, 0); //timeout required so Formsy react apply error is called first
  };

  render() {
    return (
      <Fragment>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container">
            <a className="navbar-brand" href="/">
              <img
                src={logo}
                className="d-inline-block align-top"
                alt="Totally Money"
                height="30"
              />
            </a>
          </div>
        </nav>
        <div className="container">
          <ApplicationForm onSubmit={this.onSubmit} />
          <Results results={this.state.results} />
        </div>
      </Fragment>
    );
  }
}
