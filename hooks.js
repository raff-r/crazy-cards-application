const { BeforeAll, AfterAll } = require('cucumber');
const { client } = require('nightwatch-cucumber');

BeforeAll(function() {

  const domain = client.launchUrl.indexOf('localhost') !== -1 ? '' : '.moneysavingexpert.com';

  client
    .url(client.launchUrl)
    .setCookie({
      name: 'tip',
      value: 'tip',
      domain: domain,
      path: '/',
      expiry: '636388492'
    }, function() {
      console.log('cookie set');
    })

});

AfterAll(function () {
  client
    .deleteCookies()
    .refresh();
});